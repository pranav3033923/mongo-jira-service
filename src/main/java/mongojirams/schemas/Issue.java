package mongojirams.schemas;

public class Issue {
    private String outwardIssueKey;
    private String typeOutward;

    public Issue(String outwardIssueKey, String typeOutward) {
        this.outwardIssueKey = outwardIssueKey;
        this.typeOutward = typeOutward;
    }

    public String getOutwardIssueKey() {
        return outwardIssueKey;
    }

    public void setOutwardIssueKey(String outwardIssueKey) {
        this.outwardIssueKey = outwardIssueKey;
    }

    public String getTypeOutward() {
        return typeOutward;
    }

    public void setTypeOutward(String typeOutward) {
        this.typeOutward = typeOutward;
    }
}
