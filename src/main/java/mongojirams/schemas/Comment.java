package mongojirams.schemas;

public class Comment {
    private String authorDisplayName;
    private String authorEmailAddress;
    private String body;

    public String getAuthorDisplayName() {
        return authorDisplayName;
    }

    public void setAuthorDisplayName(String authorDisplayName) {
        this.authorDisplayName = authorDisplayName;
    }

    public String getAuthorEmailAddress() {
        return authorEmailAddress;
    }

    public void setAuthorEmailAddress(String authorEmailAddress) {
        this.authorEmailAddress = authorEmailAddress;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Comment(String authorDisplayName, String authorEmailAddress, String body) {
        this.authorDisplayName=authorDisplayName;
        this.authorEmailAddress=authorEmailAddress;
        this.body=body;
    }
}
