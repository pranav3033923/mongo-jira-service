package mongojirams.schemas;

import org.bson.Document;

import java.util.List;

public class Schema {
    private String id;
    private String projectId;
    private String creatorDisplayName;
    private String creatorEmailAddress;
    private String reporterDisplayName;
    private String reportEmailAddress;
    private Integer progressProgress;
    private Integer progressTotal;
    private String updated;
    private String description;
    private String summary;
    private String statusName;
    private String statusCategory;


    private List<Document> attachment;
    private List<Document> linkedissues;
    private List<Document> comments;
    private String priority;
    private String created;


    public Schema(String id, String projectId, String creatorDisplayName, String creatorEmailAddress, String reporterDisplayName, String reportEmailAddress, Integer progressProgress, Integer progressTotal, String updated, String description, String summary, String statusName, String statusCategory, List<Document> attachment, List<Document> linkedissues, List<Document> comments, String priority, String created) {
        this.id = id;
        this.projectId = projectId;
        this.creatorDisplayName = creatorDisplayName;
        this.creatorEmailAddress = creatorEmailAddress;
        this.reporterDisplayName = reporterDisplayName;
        this.reportEmailAddress = reportEmailAddress;
        this.progressProgress = progressProgress;
        this.progressTotal = progressTotal;
        this.updated = updated;
        this.description = description;
        this.summary = summary;
        this.statusName = statusName;
        this.statusCategory = statusCategory;
        this.attachment = attachment;
        this.linkedissues = linkedissues;
        this.comments = comments;
        this.priority = priority;
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCreatorDisplayName() {
        return creatorDisplayName;
    }

    public void setCreatorDisplayName(String creatorDisplayName) {
        this.creatorDisplayName = creatorDisplayName;
    }

    public String getCreatorEmailAddress() {
        return creatorEmailAddress;
    }

    public void setCreatorEmailAddress(String creatorEmailAddress) {
        this.creatorEmailAddress = creatorEmailAddress;
    }

    public String getReporterDisplayName() {
        return reporterDisplayName;
    }

    public void setReporterDisplayName(String reporterDisplayName) {
        this.reporterDisplayName = reporterDisplayName;
    }

    public String getReportEmailAddress() {
        return reportEmailAddress;
    }

    public void setReportEmailAddress(String reportEmailAddress) {
        this.reportEmailAddress = reportEmailAddress;
    }

    public Integer getProgressProgress() {
        return progressProgress;
    }

    public void setProgressProgress(Integer progressProgress) {
        this.progressProgress = progressProgress;
    }

    public Integer getProgressTotal() {
        return progressTotal;
    }

    public void setProgressTotal(Integer progressTotal) {
        this.progressTotal = progressTotal;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getStatusCategory() {
        return statusCategory;
    }

    public void setStatusCategory(String statusCategory) {
        this.statusCategory = statusCategory;
    }

    public List<Document> getAttachment() {
        return attachment;
    }

    public void setAttachment(List<Document> attachment) {
        this.attachment = attachment;
    }

    public List<Document> getLinkedissues() {
        return linkedissues;
    }

    public void setLinkedissues(List<Document> linkedissues) {
        this.linkedissues = linkedissues;
    }

    public List<Document> getComments() {
        return comments;
    }

    public void setComments(List<Document> comments) {
        this.comments = comments;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
}

