package mongojirams;


import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.apache.logging.log4j.ThreadContext;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import mongojirams.schemas.Comment;
import mongojirams.schemas.Issue;
import mongojirams.schemas.Schema;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



@SpringBootApplication(exclude = {
        MongoAutoConfiguration.class,
        MongoDataAutoConfiguration.class
})
@RestController
public class Mongoapi {

    // Logger to give more customized logs for further usage
    private static final Logger logger = LogManager.getLogger(Mongoapi.class);


    private static final String SECRETS_FILE = "secrets.properties";
    private static final String JIRA_URL = "jira.url";
    private static final String API_KEY = "api.key";
    private static final String MONGO_URL = "mongo.url";
    private static final String COLL_NAME = "collection.name";
    private static final String DB_NAME = "db.name";
    private static String JIRAURL="";
    private static String APIKEY="";

    public String updateMongo (){
        // Loading secrets from src/resources/secrets.properties

        Properties properties = loadProperties();
        JIRAURL=properties.getProperty(JIRA_URL);
        APIKEY=properties.getProperty(API_KEY);
        String MONGODB_URI=properties.getProperty(MONGO_URL);
        String COLLECTION_NAME=properties.getProperty(COLL_NAME);
        String DATABASE_NAME=properties.getProperty(DB_NAME);


        try {
            // fetching data from JIRA API
            String apiData = fetchDataFromAPI();

            List<Document> documents = new ArrayList<>();
            documents = (List<Document>) Document.parse(apiData).get("issues");

            // Connecting to MongoDB
            MongoClientURI connectionString = new MongoClientURI(MONGODB_URI);
            try (MongoClient mongoClient = new MongoClient(connectionString)) {
                MongoDatabase database = mongoClient.getDatabase(DATABASE_NAME);
                MongoCollection<Document> collection = database.getCollection(COLLECTION_NAME);

//
                for (Document document : documents) {
                    // Performing computation on fetched documents
                    List<Document> comments = new ArrayList<>();
                    List<Document> issues = new ArrayList<>();
                    List<Document> attachments = new ArrayList<>();
                    try {
                        String commentsData = fetchCommentFromAPI(document.getString("self"));
                        if(commentsData.length()>0) {
                            List<Document> tempComments = new ArrayList<>();
                            List<Document> tempIssues = new ArrayList<>();
                            List<Document> tempattachs = new ArrayList<>();
                            tempComments = (List<Document>) Document.parse(commentsData).get("fields", Document.class).get("comment", Document.class).get("comments");
                            tempIssues = (List<Document>) document.get("fields", Document.class).get("issuelinks");
                            tempattachs = (List<Document>) document.get("fields", Document.class).get("attachment");
                            if (tempComments != null) {
                                for (Document obj : tempComments) {
                                    Comment tempComment = new Comment(obj.get("author", Document.class).get("displayName", ""), obj.get("author", Document.class).get("emailAddress", ""), obj.get("body", ""));
                                    Document commentStorage = new Document("authorDisplayName", tempComment.getAuthorDisplayName()).append("authorEmailAddress", tempComment.getAuthorEmailAddress()).append("body", tempComment.getBody());
                                    comments.add(commentStorage);
                                }
                            }

                            if (tempIssues != null) {
                                for (Document obj : tempIssues) {
                                    Issue tempIssue = new Issue(obj.get("outwardIssue", Document.class).get("key", ""), obj.get("type", Document.class).get("outward", ""));
                                    Document issueStorage = new Document("outwardIssueKey", tempIssue.getOutwardIssueKey()).append("typeOutward", tempIssue.getTypeOutward());
                                    issues.add(issueStorage);
                                }
                            }
                            List<String> attachment;
                            if (tempattachs != null) {
                                for (Document obj : tempattachs) {
                                    Document attachStorage = new Document("thumbnail", obj.getString("thumbnail"));
                                    attachments.add(attachStorage);
                                }
                            }
                        }

                    } catch (Exception e) {
                        ThreadContext.put("statusCode","400");
                        logger.error("Service JIRA - MONGO FAILED - An error occurred while performing computation on comments fetched from JIRA API/fetching data from JIRA API");
                        ThreadContext.clearMap();
                    }


                    Schema schema = new Schema(
                            document.get("id", ""),
                            document.get("fields", Document.class).get("project", Document.class).getString("id"),
                            document.get("fields", Document.class).get("creator", Document.class).getString("displayName"),
                            document.get("fields", Document.class).get("creator", Document.class).getString("emailAddress"),
                            document.get("fields", Document.class).get("reporter", Document.class).getString("displayName"),
                            document.get("fields", Document.class).get("reporter", Document.class).getString("emailAddress"),
                            document.get("fields", Document.class).get("progress", Document.class).getInteger("progress"),
                            document.get("fields", Document.class).get("progress", Document.class).getInteger("total"),
                            document.get("fields", Document.class).getString("updated"),
                            document.get("fields", Document.class).get("description", ""),
                            document.get("fields", Document.class).get("summary", ""), document.get("fields", Document.class).get("status", Document.class).getString("name"), document.get("fields", Document.class).get("status", Document.class).get("statusCategory", Document.class).getString("name"), attachments, issues, comments,
                            document.get("fields", Document.class).get("priority", Document.class).get("name", ""),
                            document.get("fields", Document.class).get("created", "")
                    );

                    Document doc = new Document("id", schema.getId())
                            .append("projectId", schema.getProjectId())
                            .append("creatorDisplayName", schema.getCreatorDisplayName())
                            .append("creatorEmailAddress", schema.getCreatorEmailAddress())
                            .append("reporterDisplayName", schema.getReporterDisplayName())
                            .append("reporterEmailAddress", schema.getReportEmailAddress())
                            .append("progress", schema.getProgressProgress())
                            .append("total", schema.getProgressTotal())
                            .append("updated", schema.getUpdated())
                            .append("description", schema.getDescription())
                            .append("summary", schema.getSummary())
                            .append("comments", schema.getComments())
                            .append("priority", schema.getPriority())
                            .append("created", schema.getCreated())
                            .append("statusName", schema.getStatusName()).append("statusCategoryName", schema.getStatusCategory()).append("attachments", schema.getAttachment()).append("issues", schema.getLinkedissues());

                    // Logic to find whether that doc exists or not, if not create new, else update
                    Bson filter = Filters.eq("id", doc.get("id"));
                    Document existingDocument = collection.find(filter).first();
                    if (existingDocument != null && !existingDocument.equals(doc)) {
                        collection.replaceOne(filter, doc);
                    } else if (existingDocument == null) {
                        collection.insertOne(doc);
                    }
                    ThreadContext.put("statusCode","200");
                    logger.info("Service JIRA - MONGO SUCCESS:3.0 : Document[--] saved to MongoDB successfully !!");
                    ThreadContext.clearMap();

                }

            }
            catch (Exception e){
                ThreadContext.put("statusCode","400");
                logger.error("Service JIRA - MONGO Failed - An error occurred while connecting to MongoDB");
                ThreadContext.clearMap();
                throw new RuntimeException("Service JIRA - MONGO Failed - An error occurred while connecting to MongoDB");
            }


        } catch (Exception e) {
            ThreadContext.put("statusCode","400");
            logger.error("Service JIRA - MONGO Failed - An error occurred while fetching DATA from JIRA API/MONGODB Connect");
            ThreadContext.clearMap();
            throw new RuntimeException("Service JIRA - MONGO Failed - An error occurred while fetching DATA from JIRA API/MONGODB Connect");
        }

        return "Service JIRA - MONGO successfully completed !!";

    }

    private static Properties loadProperties() {
        // Props load from src/resources/secrets.properties
        Properties properties = new Properties();
        try (InputStream inputStream = Mongoapi.class.getClassLoader().getResourceAsStream(SECRETS_FILE)) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
            ThreadContext.put("statusCode","500");
            logger.error("Service JIRA - MONGO Failed - An error occurred while extracting secrets from .prop file");
            ThreadContext.clearMap();
            throw new RuntimeException("Service JIRA - MONGO Failed - An error occurred while extracting secrets from .prop file");

        }
        return properties;
    }

    private static String fetchDataFromAPI() throws IOException, InterruptedException {
        // Fetch data from the API -- all
        String url = JIRAURL;
        String token = APIKEY;

        HttpClient client = HttpClient.newBuilder().build();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Authorization", "Basic " + token)
                .build();

        try {
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            ThreadContext.put("statusCode","200");
            logger.info("Service JIRA - MONGO SUCCESS:1.0 : Data from JIRA API retrieved successfully !!");
            ThreadContext.clearMap();
            return response.body();

        }catch (Exception e){
            ThreadContext.put("statusCode","400");
            logger.error("Service JIRA - MONGO Failed - An error received while fetching data from JIRA API");
            ThreadContext.clearMap();
            throw new RuntimeException("Service JIRA - MONGO Failed - An error occurred while fetching data from JIRA API");
        }



    }


    private static String fetchCommentFromAPI(String url) throws IOException, InterruptedException {
        // Fetch data from the API -- only comments of an issue
        String token = APIKEY;


            HttpClient client = HttpClient.newBuilder().build();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(url))
                    .header("Authorization", "Basic " + token)
                    .build();

            try {
                HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
                ThreadContext.put("statusCode","200");
                logger.info("Service JIRA - MONGO SUCCESS:2.0 : Comment[--] from JIRA API retrieved successfully !!");
                ThreadContext.clearMap();
                return response.body();

            }catch (Exception e){
                ThreadContext.put("statusCode","400");
                logger.error("Service JIRA - MONGO Failed - An error received while fetching comments from JIRA API");
                ThreadContext.clearMap();
                return "";

            }

    }

    @GetMapping("/fetch-data")
    //fetch request to start the application/container
    public String fetchData() {
        try {
            String responseMONGOJIRA = updateMongo();
            ThreadContext.put("statusCode","200");
            logger.info("Service JIRA - MONGO SUCCESS");
            ThreadContext.clearMap();
            return responseMONGOJIRA;
            
        } catch (Exception e) {
            ThreadContext.put("statusCode","400");
            logger.error("Service JIRA - MONGO FAILED - An error occurred in the service JIRA - MONGO while performing GET request !!");
            ThreadContext.clearMap();
            return "Error occurred while fetching data (Service: JIRA - MONGO)";
        }
    }



    public static void main(String[] args) {
        SpringApplication.run(Mongoapi.class, args);
    }
}