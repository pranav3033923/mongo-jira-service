# JIRA - Mongo Service
Service that fetches data from JIRA and stores in MongoDB

## Implementation
...
- Service is written in Java Maven + Spring Boot framework is opted
- Data is fetched from JIRA API & stored in MongoDB
...
- MongoDB detects that modification using ChangeStreamDocument & forwards it to Kafka by acting as Kafka Producer
- Kafka Streams are opted for continuous data capturing
- Confluent Cloud is opted rather than working locally to reduce the latency : after deployment on Kubernetes
- ElasticSearch updates itself by capturing documents (acting as Kafka Consumer)
- Elastic Cloud is used for storage & Kibana for testing

## DEMO VIDEO

[JIRA - Mongo Service - Demonstration][1]

[1]: https://sprinklr-my.sharepoint.com/:v:/p/pranav1/EXp9X7W1yE1BlVSNjFpoFS4BqJQLQeDje0zeHNWVI9hWTg?e=5Dy1TW

## Google Cloud Registery - Image

Image of JIRA - Mongo Service

```bash
  gcr.io/hopeful-altar-392410/mongojiraservice
```

![Logo](https://i.ibb.co/SrVGdPY/Sprinklr-Logo-Primary-Use-Positive-RBG.jpg)


